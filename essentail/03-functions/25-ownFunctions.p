routine-level on error undo, throw.
define variable iResult as integer no-undo.

/* Function Header Block */
function sumNumbers return integer
   (input a as integer, input b as integer, output iResult as integer)
   forward.

/* Main Block */
message sumNumbers(input 10, input 20, output iResult)
   view-as alert-box info buttons ok.

message iResult
   view-as alert-box info buttons ok.

/* Main Block */

/* Function Block */
function sumNumbers return integer
   (input a as integer, input b as integer, output iResult as integer):

   iResult = a + b.
   return iResult.
end function.
