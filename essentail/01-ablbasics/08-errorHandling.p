routine-level on error undo, throw.
define variable iTest as integer no-undo.
iTest = integer("Something not integer") no-error.

if error-status:error then
   message "Avem o eroare:" error-status:get-message (1)
   view-as alert-box.


find first Customer where CustNum > 10000 no-lock.
/*find first Customer where CustNum > 10000 no-lock no-error.*/

if error-status:error then
   message "Avem o eroare:" error-status:get-message (1)
      view-as alert-box.