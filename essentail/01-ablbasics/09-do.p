routine-level on error undo, throw.

define variable iTest as integer no-undo.

do iTest = 1 to 10:
   display iTest.
   pause.
end.

unFor:
for each Customer no-lock where Customer.CustNum < 10:
   display Customer except Comments.

   if Customer.State <> "NY" then
   do:
      leave unFor.
   end.
end.