OpenEdge Documentation and Samples for Progress OpenEdge


INSTALLING THE DOCUMENTATION AND SAMPLES:
-----------------------------------------
The Documentation and Samples provides:

- OpenEdge PDF documentation (including OpenEdge Replication and OpenEdge Management and OpenEdge Explorer).
- OpenEdge sample files.



Installing on Windows Platforms
-------------------------------
In Windows platforms install the Documentation and Samples as follows:

Note: If you want the sample files to install into your Progress OpenEdge installation directory, you must have already installed Progress OpenEdge. Otherwise, the samples will only be installed in the Documentation and Samples installation location.

1. Run setup.exe file (extracted from the Documentation and Samples .exe file downloaded from the Progress ESD site).

2. In the dialog box that appears, select which components you want to install.

3. Choose the installation directory, or select the default, such as C:\Progress\OpenEdge_nn_Doc, where nn is the OpenEdge release number.

After you have completed the installation, access the documentation files by selecting Start->Programs->OpenEdge Documentation.


Installing on UNIX Platforms
----------------------------
On UNIX platforms install the Documentation and Samples as follows:


1. Create a folder on the machine where you want the documentation and samples to reside, such as OpenEdge_114_Doc.

2. Copy the OpenEdge_Doc folder that was extracted from the .tar file that you downloaded from the Progress ESD site to the location you created in Step 1. If you do not want to install all of the documentation and samples in the OpenEdge_Doc folder, just copy the folders you want:

- openedge- contains the OpenEdge documentation.

- src, tutorial, and webinstall - contain the sample code and documentation example files.  You may also want to copy these directories to the equivalent locations in your Progress OpenEdge installation directory. 

After you have completed the installation, you can access the documentation by opening the Documentation Welcome page located in the following directory:

OpenEdge_Doc/openedge/start.pdf 


Viewing documentation directly from the downloaded ESD files
-------------------------------------------------------------
If you do not want to install the documentation, you can view the PDF documentation files directly from the files that were downloaded from the ESD site. To do so, extract the file and browse to the following directory and open the Welcome page (start.pdf) for the documentation set:

- OpenEdge_Doc/openedge/start.pdf



Downloading the Latest Acrobat Reader
-------------------------------------

For best results when viewing the PDF files, install the latest Adobe Reader.  You can download the latest Adobe Reader for your platform from the following URL:  http://get.adobe.com/reader/
