DEFINE VARIABLE  ii AS INTEGER INIT 1.

DO ON ERROR UNDO:
    ii = 2.
     
    FIND FIRST customer WHERE customer.custnum = 10000.
END.

MESSAGE 'after' SKIP
    ii
    VIEW-AS ALERT-BOX INFO BUTTONS OK.
