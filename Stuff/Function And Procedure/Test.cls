
/*------------------------------------------------------------------------
    File        : Test
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : wayfareadm
    Created     : Mon Mar 27 11:43:59 EEST 2017
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.

INTERFACE Test: 
   METHOD PUBLIC CHARACTER getText(OUTPUT a AS CHARACTER).
  
END INTERFACE.