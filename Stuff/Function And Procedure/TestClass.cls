 
 /*------------------------------------------------------------------------
    File        : TestClass
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : wayfareadm
    Created     : Mon Mar 27 11:48:52 EEST 2017
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS TestClass IMPLEMENTS Test: 

	METHOD PUBLIC CHARACTER getText(OUTPUT a AS CHARACTER ):
		
/*		UNDO, THROW NEW Progress.Lang.AppError("METHOD NOT IMPLEMENTED").*/ 
        a = "Hello form TestClass".
       END METHOD.
      
	/*------------------------------------------------------------------------------
	 Purpose:
	 Notes:
	------------------------------------------------------------------------------*/
		
	CONSTRUCTOR PUBLIC TestClass (  ):
		SUPER ().
		
	END CONSTRUCTOR.

	/*------------------------------------------------------------------------------
	 Purpose:
	 Notes:
	------------------------------------------------------------------------------*/
	DESTRUCTOR PUBLIC TestClass ( ):

	END DESTRUCTOR.


END CLASS.