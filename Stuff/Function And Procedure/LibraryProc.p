
/*------------------------------------------------------------------------
    File        : LibraryProc.p
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : 
    Created     : Fri Mar 24 17:24:11 EET 2017
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

BLOCK-LEVEL ON ERROR UNDO, THROW.

FUNCTION addTwo RETURN INTEGER(INPUT a AS INTEGER)FORWARD.
FUNCTION repp RETURN CHARACTER(INPUT a AS CHARACTER ) FORWARD.

/* ********************  Preprocessor Definitions  ******************** */

/* ***************************  Main Block  *************************** */


FUNCTION addTwo RETURN INTEGER(INPUT a AS INTEGER):        
    RETURN a + 2.
END FUNCTION.


PROCEDURE messagee:
    DEFINE INPUT  PARAMETER inPara AS INTEGER NO-UNDO.
    MESSAGE inPara
    VIEW-AS ALERT-BOX.
 END PROCEDURE.
 
 FUNCTION repp RETURN CHARACTER(INPUT a AS CHARACTER):
     
     RETURN REPLACE(a,"a","b").
     
 END FUNCTION.
 
MESSAGE  repp("ana")
VIEW-AS ALERT-BOX.

