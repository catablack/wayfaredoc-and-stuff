
/*------------------------------------------------------------------------
    File        : main.p
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : 
    Created     : Fri Mar 24 17:38:10 EET 2017
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

BLOCK-LEVEL ON ERROR UNDO, THROW.

/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */


DEFINE VARIABLE hLibrary AS HANDLE NO-UNDO.
DEFINE VARIABLE hProc AS HANDLE NO-UNDO.
DEFINE VARIABLE cText AS CHARACTER INITIAL "ABCD;EFGH;IJKL" NO-UNDO.
DEFINE VARIABLE indexx AS INTEGER NO-UNDO.

DEFINE VARIABLE cla AS CLASS Test NO-UNDO.
 cla =NEW TestClass().
cla:getText(cText).

MESSAGE cText
VIEW-AS ALERT-BOX.
DELETE OBJECT cla NO-ERROR.

/*RUN Libraryproc.p PERSISTENT SET hLibrary.              */
/*RUN internal.p PERSISTENT SET hProc.                    */
/*                                                        */
/*/*Display the time*/                                    */
/*                                                        */
/*MESSAGE STRING(TIME,"HH:MM:SS")                         */
/*VIEW-AS ALERT-BOX.                                      */
/*                                                        */
/*DO indexx = 1 TO NUM-ENTRIES(cText,";"):                */
/*    MESSAGE ENTRY(indexx,cText,";")                     */
/*    VIEW-AS ALERT-BOX.                                  */
/*END.                                                    */
/*                                                        */
/*IF LOOKUP("ABCD",cText,";") > 0                         */
/*THEN                                                    */
/*    MESSAGE "Da"                                        */
/*    VIEW-AS ALERT-BOX.                                  */
/*DO indexx=1 TO 5:                                       */
/*   MESSAGE RANDOM(1,indexx * 100000 + TIME )            */
/*   VIEW-AS ALERT-BOX.                                   */
/*   END.                                                 */
/*                                                        */
/*MESSAGE DYNAMIC-FUNCTION("addTwo" IN hLibrary ,INPUT 12)*/
/*VIEW-AS ALERT-BOX BUTTON OK.                            */
/*                                                        */
/*RUN displayy IN hProc (INPUT "Hello form procedure").   */
/*                                                        */
/*FINALLY:                                                */
/*    DELETE OBJECT  hLibrary NO-ERROR.                   */
/*    DELETE OBJECT hProc NO-ERROR.                       */
/*END FINALLY.                                            */


